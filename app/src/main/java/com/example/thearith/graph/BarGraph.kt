package com.example.thearith.graph

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Paint.ANTI_ALIAS_FLAG
import android.support.v4.content.ContextCompat
import android.text.TextPaint
import android.util.AttributeSet
import android.view.View
import android.view.animation.AccelerateInterpolator

class BarGraph(context: Context, attrs: AttributeSet? = null): View(context, attrs) {

    private val barWidth: Int
    private val hasGuideline: Boolean
    private val maxBarSpacing = context.resources.getDimensionPixelSize(R.dimen.max_bar_spacing)
    private val barTextPadding = context.resources.getDimensionPixelSize(R.dimen.bar_text_padding)
    private val barTextSize: Int

    private val gridPaint = Paint(ANTI_ALIAS_FLAG)
    private val guidelinePaint = Paint(ANTI_ALIAS_FLAG)
    private val barPaint = Paint(ANTI_ALIAS_FLAG)
    private val barTextPaint = TextPaint(ANTI_ALIAS_FLAG)

    private val valueAnimator = ValueAnimator().apply {
        duration = 500
        interpolator = AccelerateInterpolator()
        addUpdateListener { animation ->
            animatingFraction = animation.animatedFraction
            invalidate()
        }
        setFloatValues(0f, 1f)
    }
    private var animatingFraction: Float = 0f

    private var data: List<Float> = emptyList()
    private var percents: List<Float> = emptyList()

    init {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.BarGraph, 0, 0)
        barWidth = typedArray.getDimensionPixelSize(context, R.styleable.BarGraph_barWidth,
                R.dimen.default_bar_width)
        hasGuideline = typedArray.getBoolean(R.styleable.BarGraph_hasGuideline, true)
        barPaint.color = typedArray.getColor(context, R.styleable.BarGraph_barColor,
                R.color.default_bar_color)

        barTextSize = typedArray.getDimensionPixelSize(context, R.styleable.BarGraph_barTextSize,
                R.dimen.default_bar_text_size)
        barTextPaint.color = typedArray.getColor(context, R.styleable.BarGraph_barTextColor,
                R.color.default_bar_text_color)
        barTextPaint.textSize = barTextSize.toFloat()
        barTextPaint.textAlign = Paint.Align.CENTER

        typedArray.recycle()
    }

    fun setData(percents: List<Float>) {
        this.data = percents
        this.percents = percents.toBarGraphData()
        startAnimate()
    }

    fun startAnimate() {
        animatingFraction = 0f
        valueAnimator.start()
    }

    override fun onDraw(canvas: Canvas) {
        val gridLeft = paddingLeft.toFloat()
        val gridTop = paddingTop.toFloat()
        val gridRight = width.toFloat() - paddingRight
        val gridBottom = height.toFloat() - paddingBottom

        // Draw grids
        canvas.drawLine(gridLeft, gridTop, gridLeft, gridBottom, gridPaint)
        canvas.drawLine(gridLeft, gridBottom, gridRight, gridBottom, gridPaint)

        // Draw guide lines
        if(hasGuideline) {
            val gridSpacing = (gridBottom - gridTop) / 10
            for (i in 0..9) {
                val top = gridTop + gridSpacing * i
                canvas.drawLine(gridLeft, top, gridRight, top, guidelinePaint)
            }
        }

        // Draw bars
        if(percents.isEmpty()) {
            return
        }

        val calculatedBarSpacing = (gridRight - gridLeft - barWidth * percents.size) /
                percents.size
        val barSpace = Math.min(calculatedBarSpacing, maxBarSpacing.toFloat())
        val maxBarTop = gridTop + barTextSize + 2 * barTextPadding
        for(i in 0 until percents.size) {
            val barLeft = gridLeft + barSpace * (i+1) + barWidth * i
            val barRight = barLeft + barWidth
            val barTop = maxBarTop +
                    (gridBottom - maxBarTop) * (1 - percents[i] * animatingFraction)
            canvas.drawRect(barLeft, barTop, barRight, gridBottom, barPaint)

            // draw value
            val value = data[i].toInt().toString()
            canvas.drawText(
                    value,
                    (barLeft + barRight) / 2,
                    barTop - barTextPadding,
                    barTextPaint
            )
        }

        super.onDraw(canvas)
    }
}

fun List<Float>.toBarGraphData(max: Float? = max()): List<Float> {
    val maxValue = max ?: return emptyList()
    return map { value -> value / maxValue }
}