package com.example.thearith.graph

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.support.v4.content.ContextCompat
import android.text.TextPaint
import android.util.AttributeSet
import android.view.View

private const val MAX_VALUE = 10F

class LineGraph(context: Context, attrs: AttributeSet? = null): View(context, attrs) {

    private val gridPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val guidelinePaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val linePaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val pointPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val labelTextPaint = TextPaint(Paint.ANTI_ALIAS_FLAG)
    private val hLabelTextPaint = TextPaint(Paint.ANTI_ALIAS_FLAG)

    private val labelTextSize: Int
    private val labelTextPadding = context.resources.getDimensionPixelSize(R.dimen.line_text_padding)
    private val textBounds = Rect()
    private val pointRadius = context.resources.getDimensionPixelSize(R.dimen.line_point_radius)
    private val strokeWidth = context.resources.getDimensionPixelSize(R.dimen.line_point_stroke)
    private val pointSize = pointRadius + strokeWidth

    private var data: List<Float> = emptyList()
    private var percents: List<Float> = emptyList()

    init {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.LineGraph, 0, 0)

        labelTextSize = typedArray.getDimensionPixelSize(context, R.styleable.LineGraph_labelTextSize,
                R.dimen.default_line_text_size)
        labelTextPaint.textSize = labelTextSize.toFloat()
        labelTextPaint.textAlign = Paint.Align.CENTER

        hLabelTextPaint.textSize = labelTextSize.toFloat()

        val lineColor = ContextCompat.getColor(context, R.color.default_line_color)
        linePaint.color = lineColor
        linePaint.strokeWidth = strokeWidth.toFloat()
        pointPaint.color = Color.WHITE

        typedArray.recycle()
    }

    fun setData(data: List<Float>) {
        this.data = data
        this.percents = data.toBarGraphData(MAX_VALUE)
        invalidate()
    }

    override fun onDraw(canvas: Canvas) {
        if(percents.isEmpty()) return

        val textGap = labelTextSize + labelTextPadding
        val gridLeft = paddingLeft.toFloat() + textGap + labelTextPadding
        val gridTop = paddingTop.toFloat() + textGap
        val gridRight = width.toFloat() - (paddingRight + textGap)
        val gridBottom = height.toFloat() - (paddingBottom + textGap + labelTextPadding)

        // Draw Grid
        canvas.drawLine(gridLeft, gridTop, gridLeft, gridBottom, gridPaint)
        canvas.drawLine(gridLeft, gridBottom, gridRight, gridBottom, gridPaint)
        canvas.drawText("0", gridLeft - textGap, gridBottom + textGap,
                hLabelTextPaint)

        // Draw horizontal guidelines
        val vGridSpacing = (gridBottom - gridTop) / 5
        for (index in 1..5) {
            val yPos = gridBottom - vGridSpacing * index
            canvas.drawLine(gridLeft, yPos, gridRight, yPos, guidelinePaint)

            val label = 2 * index
            canvas.drawText(
                    label.toString(),
                    gridLeft - textGap,
                    yPos,
                    hLabelTextPaint
            )
        }

        val size = percents.size
        val hGridSpacing = (gridRight - gridLeft) / size
        for(index in 0 until size) {
            // Draw guideline and label
            val xPos = gridLeft + hGridSpacing * (index + 1)
            canvas.drawLine(xPos, gridTop, xPos, gridBottom, guidelinePaint)
            val label = (index + 1).toString()
            canvas.drawText(
                    label,
                    xPos,
                    gridBottom + textGap,
                    labelTextPaint
            )

            val height = gridBottom - (gridBottom - gridTop) * percents[index]

            // draw line
            if(index != percents.size - 1) {
                val nextHeight = gridBottom - (gridBottom - gridTop) * percents[index + 1]
                val nextXPos = gridLeft + hGridSpacing * (index + 2)
                canvas.drawLine(
                        xPos,
                        height,
                        nextXPos,
                        nextHeight,
                        linePaint
                )
            }

            // draw point
            canvas.drawCircle(xPos, height, pointSize.toFloat(), linePaint)
            canvas.drawCircle(xPos, height, pointRadius.toFloat(), pointPaint)
        }

        super.onDraw(canvas)
    }
}