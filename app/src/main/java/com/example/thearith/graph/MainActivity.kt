package com.example.thearith.graph

import android.databinding.DataBindingUtil
import android.os.Bundle
import kotlin.LazyThreadSafetyMode.NONE
import android.support.v7.app.AppCompatActivity
import com.example.thearith.graph.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private val mainViewModel by lazy(NONE) { MainViewModel() }
    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityMainBinding>(
                this,
                R.layout.activity_main
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.model = mainViewModel
    }

}

data class MainViewModel(
        val barData: List<Float> = listOf(1f, 3f, 5f, 4f, 7f, 6f),
        val lineData: List<Float> = listOf(1f, 3f, 5f, 4f, 7f, 6f)
)