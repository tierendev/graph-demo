package com.example.thearith.graph

import android.content.Context
import android.content.res.TypedArray
import android.support.annotation.ColorRes
import android.support.annotation.DimenRes
import android.support.annotation.StyleableRes
import android.support.v4.content.ContextCompat

fun TypedArray.getDimensionPixelSize(
        context: Context,
        @StyleableRes styleableRes: Int,
        @DimenRes defaultDimen: Int
) = getDimensionPixelSize(styleableRes, context.resources.getDimensionPixelSize(defaultDimen))

fun TypedArray.getColor(
        context: Context,
        @StyleableRes styleableRes: Int,
        @ColorRes defaultColor: Int
) = getColor(styleableRes, ContextCompat.getColor(context, defaultColor))